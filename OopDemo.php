<?php

//PARENT CLASS
class ParentClass{

    //CONSTANT VARIABLE DEFINED
    const newValue="Constant value does not consist $ sign";
    //STATIC VARIABLE DEFINED
    private static $var=0;

    //CREATE CONSTRUCTOR AND IT CALLS ITSELF WHEN CREATE AN OBJECT
    function __construct(){

        self::$var++;
        print "<br/>In parent class <br/>";
        print self::$var. '<br/>';
    }
    //CREATE DESTRUCTOR
    function __destruct(){

        print "Parent Class Destructor called<br/>";

    }
    //NORMAL FUNCTION TO DISPLAY STATIC VARIABLE
    public function disp(){

        echo "Inside the class<br/>";
        echo self::newValue."\n";
        print self::$var;

    }

}
//CHILD CLASS EXTENDS PARENT CLASS
class ChildClass extends ParentClass{

    function __construct(){
        //CALLS PARENT CLASS CONSTRUCTOR FIRST
        parent::__construct();

        print "In child class<br/>";

    }

    function __destruct(){

        print "Child Class Destructor called<br/>";
        //PRIVATE VARIABLE USED OUTSIDE OF CLASS
        print self::$var;

    }


}

$obj1=new ParentClass();
$obj1::disp();

$obj2=new ChildClass();

?>