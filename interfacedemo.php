<?php

//SHIPPING INTERFACE
interface Shipping {
        public function isShippingCharge();
        public function isTrackingAvailable();
        }

interface includeTax{
        public function includeTax();
        public function excludeTax();
}

//ORDER CLASS THAT IMPLEMENTS SHIPPING INTERFACE
class Order implements Shipping,includeTax {
        private $name;
        public function __construct($name,$age) {
                $this->name = $name;
                $this->age  = $age;
                }
        public function getname() {
                return $this->name;
                }
        public function getage() {
                return $this->age;
        }
        //INTERFACE METHODS WHICH NEEDS TO BE IMPLEMENTED
        public function isShippingCharge() {
                echo "Shipping is chargeable.<br>";
        }
         public function isTrackingAvailable() {
                echo "Tracking is Available<br>";
        }
         public function includeTax()
         {
                echo "text included<br>";
         }
        public function excludeTax()
        {
                echo "text excluded<br>";
        }
}

//PRODUCT CLASS THAT IMPLEMENTS SHIPPING INTERFACE
class Product implements Shipping {
        private $name;
        public function __construct($name,$price) {
                $this->name = $name;
                $this->price  = $price;
                }
        public function getname() {
                return $this->name;
                }
         public function getprice() {
                return $this->price;
                }
        //INTERFACE METHODS WHICH NEEDS TO BE IMPLEMENTED
         public function isShippingCharge() {
                echo "Shipping is chargeable.<br>";
        }
         public function isTrackingAvailable() {
                echo "Tracking is Available<br>";
        }

}
?>
<html>
<body><h1>Interface code</h1>

<?php

$charlie  = new Order("Charlie",25);
$Laptop   = new Product("Laptop",45000);

$custage  = $charlie -> getage();
$custname = $charlie -> getname();
           

$proname  = $Laptop  -> getname();
$proprice = $Laptop  -> getprice();
            

print "$custname is $custage years old!<br><br>";
print "$proname is for $proprice ruppes only!<br><br>";

//CALLED INTERFACES METHODS
$charlie -> isShippingCharge();
$charlie -> includeTax();
$Laptop  -> isTrackingAvailable();

echo "<br>";
echo ($charlie instanceof Order)? "Charlie is a Customer<br>":"Charlie is a Product<br>";
echo ($Laptop instanceof Product)? "Laptop is a Product<br>":"Laptop is a Customer<br>";

?>
<hr>
</body>
</html>