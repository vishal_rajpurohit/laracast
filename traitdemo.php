<?php

//TRAIT CREATED
trait Properties{
    //TO THROW COMMON EXCEPTION IF FILE DOES NOT EXIST
    public function getfile($filename){
        if(!file_exists($filename))
        {
            throw new Exception('The file '. $filename .' does not exist');
        }

    }
   //TO FETCH COMMON TITLE TO DISPLAY ON EVERY PAGE
    public function setTitle($title){

        if(!isset($this->$title)){
            try{
                echo 'Welcome to the '.$title.'..!';
            }catch(Exception $e){
                throw new Exception('Name is not valid');
            }

        }else{
            throw new Exception('Please Enter any name');
        }
    }
}

class Some{
    //CALL TRAIT
    use Properties;

}

$s = new Some();
//CALL TRAIT FUNCTIONS
echo $s->getfile('hello.php');
echo $s->setTitle('Viitorcloud');
?>