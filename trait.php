  <?php
 
    interface Person
{
    public function greet();
    public function eat($food);
}

trait EatingTrait
{
    public function eat($food)
    {
        $this->putInMouth($food);
    }
     public function greet()
    {
        echo '1Good day, Bubye!';
    }

    private function putInMouth($food)
    {
        // digest delicious food
    }
}

class NicePerson implements Person
{
    use EatingTrait;
  
}

class MeanPerson implements Person
{
    use EatingTrait;

    public function greet()
    {
        echo '<br><br>Hello Good morning!';
    }
}

$test1 = new NicePerson();
$test2 = new MeanPerson();
$test1-> greet();
$test2-> greet();
    ?>